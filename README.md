## **Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016).**
```plantuml
@startmindmap
*[#eae4e9] Programando ordenadores en los 80 y ahora.
	*_ Son los:

		*[#cddafd] Sistemas antiguos:
			*_ Los describimos como:
				*[#f0efeb] Sistema que ya no esta a la venta o que no haya tenido una continuidad a generaciones posteriores.
				*[#f0efeb] Ordenadores de la decada de los 80's y 90's.
		*[#cddafd] Sistemas actuales:
			*_ Son
				*[#f0efeb] Los que se tiene en casa o el que se puede adquirir en alguna tienda
				*[#f0efeb] Su funcionamiento, gradualmente ira en decaida.

 
	*_ La:
 
		*[#cddafd] Programación en los 80's y 90's.
			*_ Para
				*[#f0efeb] Poder realizar la programación en diferentes maquinas, era necesario conocer la arquitectura de hardware de cada máquina.
				*[#f0efeb] Se ponía limitaciones y se tenía que resolver.
				*[#f0efeb] Los cuellos de botellas era de hardware

	*_ Las:
		*[#cddafd] Novedades de la evolución:
			*_ Son:
				*[#f0efeb] Nuevas prestaciones.
				*[#f0efeb] Precios más accesibles.
	*_ Algunas:
		*[#cddafd] Diferencias con los ordenadores actuales.
			*_ Son:
				*[#f0efeb] Potencia mucho menor.
				*[#f0efeb] La velocidad del procesador se media en MHz.
				*[#f0efeb] Tenías que un amplio conocimiento de la arquitectura.
				*[#f0efeb] El tiempo no afecta gradualmente su rendimiento.
	*_ Lenguajes de:
		*[#cddafd] Alto nivel.
			*[#f0efeb] Se tienen que realizar la traducción a código máquina.
			*[#f0efeb] Actualmente lo realizan los compiladores de intérpretes
			*[#f0efeb] Desventajas.
				* Sobrecargo para las capas realizadas.
				* Los compiladores no son tan eficientes como\n los que se generan en ensamblador.	
				* Muchas veces los programadores desconocen\nel funcionamiento del hardware.
		*[#cddafd] Bajo nivel.
			*[#f0efeb] Lo realizaba el propio programador.
			*[#f0efeb] En lenguaje ensamblador.
	*_ Las:
		*[#cddafd] Leyes:
			*[#f0efeb] Moore:
				*_ En
					* Cada 18 meses se pueden generar una CPU el doble de rápido.
			*[#f0efeb] Page
				*_ En
					* Cada 18 meses el software se vuelve el doble de lento.

@endmindmap
```
## **Historia de los algoritmos y de los lenguajes de programación (2010).**
```plantuml
@startmindmap
*[#316B83] Hª de los algoritmos y\nde los lenguajes de programación
	*_ Los
		*[#6D8299] Algoritmos
			*_ Son:
				*[#D5BFBF] Una Lista.
					* Definida
					* Ordenada
					* Finita
			*_ Permiten:
				*[#D5BFBF] Hallar la solución a un:
					*_ Problema
					*_ Estado inicial
					*_ Entrada
					*[#E6E6E6] A través de pasos sucesivos.
				*[#D5BFBF] Importancia.
					*_ Radica
						* En mostrar la manera en llevar a acabo los procesos\ny resolver mecánicamente los problemas matemáticos. 
			*_ Para ser
				*[#D5BFBF] Considerado:
					*_ Debe
						* Ser una secuencia ordenada, finita y definida de instrucciones.
			*_ Se dividen
				*[#D5BFBF] Razonables.
					*_ El
						* Tiempo de ejecución crece despacio a medida que los\nproblemas se van haciendo más grandes.
				*[#D5BFBF] No Razonables.
					*_ Son
						* Exponenciales o super polinomiales, añaden un dato mas\nal problema duplicando el tiempo.    
			
	*_ Algunos
		*[#6D8299] Antecedentes.
			*_ En
				*[#D5BFBF] Babilonia 3000 A.C
					*_ Se
						* Encontraron tablillas de arcillas para calcular el capital.
				*[#D5BFBF] Mesopotamia hace 3000 años.
					*_ Los
						* Algoritmos para cálculos de transacciones comerciales.  
				*[#D5BFBF] Siglo XVII.
					*_ Las
						* Primeras ayudas mecánicas en forma de calculador de sobremesa.
				*[#D5BFBF] Siglo XIX.
					*_ :
						* Aparece la pianola.
						* Charles babbage, diseño lo que podria ser la primera computadora de la historia.
						* Aparecen las primeras máquinas programables.
				*[#D5BFBF] Siglo XX.
					*_ A mediados
						* De este siglo aparecen los primeros ordenadores.
	*_ Los
		*[#6D8299] Lenguajes de programación
			*_ Son
				*[#D5BFBF] Son los instrumentos apropiados para\ncomunicar los algoritmos a las máquinas.
					*_ Algunos
						*[#E6E6E6] Antecedentes
							*_ En
								*[#F6F6F6] 1959.
									* FORTRAN es el primer lenguaje de alto nivel de la historia.
								*[#F6F6F6] 1960.
									* Surge Lisp, que es el representante del paradigma funcional.
								*[#F6F6F6] 1968.
									* Aparece Simula, precursor de los lenguajes orientados a objetos.
								*[#F6F6F6] 1971.
									* Aparece Prolog, lenguaje que computa a base de fórmulas lógicas.
			*_ Más 
				*[#D5BFBF] Influyentes.
					*_ Son
						*[#E6E6E6] JAVA.
						*[#E6E6E6] FORTRAN.
						*[#E6E6E6] C**

@endminmap
```

## **Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005).**

```plantuml
@startmindmap
*[#968C83] La evolución de los lenguajes y\nparadigmas de programación.
	*_ El 
		*[#D6D2C4] Ordenador
			*_ ¿Por qué se creó?
				*[#FFF5EA] Para facilitar el trabajo intelectual.
			*_ Características
				*[#FFF5EA] Trabajan en lenguaje máquina
				*[#FFF5EA] Codifican en el lenguaje binario.
				*[#FFF5EA] Se usa el ensamblador.

	*_ Los
		*[#D6D2C4] Lenguaje de programación.
			*_ Son
				*[#FFF5EA] Medio de comunicación entre el humano y la máquina.
			*_ Tienen que
				*[#FFF5EA] Adaptar a las necesidades.
				*[#FFF5EA] Aumentar la especifidad para resolver los problemas.
				*[#FFF5EA] Ofrecen más eficiencia en la ejecución
			*_ Tiene la
				*[#FFF5EA] Programación estructurada.
					*_ Consiste
						*[#89BEB3] En analizar una función principal.
						*[#89BEB3] Descomponer funciones más sencillas de resolver
					*_ Ejemplos
						*[#89BEB3] Basic.
						*[#89BEB3] C.
						*[#89BEB3] Pascal.
						*[#89BEB3] Fortran.
			*_ Tipos
				*[#FFF5EA] Concurrente.
					*_ Se
						*[#89BEB3] Usa para expresar paralelimos entre tareas 
					*_ Ejemplos
						*[#89BEB3] Sistemas automáticos.
						*[#89BEB3] Programar políticas de solución.
				*[#FFF5EA] Distribuida.
					*_ Características.
						*[#89BEB3] Ordenadores se pueden\ncomunicar entre sí.		
						*[#89BEB3] Programas que requieren\nmuchos recursos.
						*[#89BEB3] Divide el programa en \ndiversos ordenadores.
					*[#FFE2E2] Basada en\ncomponentes.
						*_ Características.
							*[#E0ECE4] Utiliza un conjunto\nde objetos.
							*[#E0ECE4] Permite la reutilización.
				*[#FFF5EA] Orientada en aspectos.
					*_ Implementa
						*[#89BEB3] Incorpora capas en base\na la necesidad del programa.
						*[#89BEB3] Permite que sean más\nlegibles y cómodo de visualizar.
						*[#89BEB3] Aspectos separados.
				*[#FFF5EA] Orientada agente software.
					*_ Son.
						*[#89BEB3] Entidades autónomas.
						*[#89BEB3] Alcanza su objetivo.
						*[#89BEB3] Multiagentes.
							*_ Se resuelven.
								*[#E0ECE4] Problemas.
								*[#E0ECE4] Dialogo.
								*[#E0ECE4] Cooperación.
								*[#E0ECE4] Coordinación.
								*[#E0ECE4] Negociación.

	*_ Un
		*[#D6D2C4] Paradigma
			*_ Es una forma
				*[#FFF5EA] De aproximarse\na la solución de un problema.
				*[#FFF5EA] De pensar o abordar el\ndesarrollo de un programa. 
			*_ Su creación.
				*[#FFF5EA] Se da por las fallas\nde la Programación\nestructurada.
			*_ De tipo
				*[#FFF5EA] Funcional.
					*_ Sus características son
						*[#89BEB3] Los programas sean\ncorrectos y verídicos.
						*[#89BEB3] Recursividad hasta\nquese cumpla cierta\ncondición.	
					*_ Ejemplos
						*[#89BEB3] ML.
						*[#89BEB3] HOPE.
						*[#89BEB3] Haskell.
						*[#89BEB3] Erlang.
				*[#FFF5EA] Lógico.
					*_ Sus caracteristicas son
						*[#89BEB3] Predicados lógicos\nque solo devuelven\ncierto o falso.
						*[#89BEB3] Recursividad.
						*[#89BEB3] No se tiene números,\nletras, ni opperaciones\narítmeticas.

@endmindmap
```
